﻿namespace Wyoming_Retirement
{
    using System.Runtime.InteropServices;
    using Microsoft.Win32.SafeHandles;
    using System.IO;
    using System.ComponentModel;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Hyland.Unity;

    public class LaunchRAIN : Hyland.Unity.IWorkflowScript
    {
        private const Diagnostics.DiagnosticsLevel DiagLevel = Diagnostics.DiagnosticsLevel.Verbose;
        private const string ScriptName = "Launch RAIN Script";

        private const String PipeName = "RAINListener";
        private const string pipePath = @"\\.\pipe\RAINListener";

        // Import possibility to gather raw access to devices
        // with global \\.\ paths which is prohibited by normal
        // .NET <cref>FileStream</cref> class.
        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern SafeFileHandle CreateFile(
            string fileName,
            [MarshalAs(UnmanagedType.U4)] FileAccess fileAccess,
            [MarshalAs(UnmanagedType.U4)] FileShare fileShare,
            IntPtr securityAttributes,
            [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
            [MarshalAs(UnmanagedType.U4)] FileAttributes fileAttributes,
            IntPtr template);

        #region IWorkflowScript
        /// <summary>
        /// Implementation of <see cref="IWorkflowScript.OnWorkflowScriptExecute" />.
        /// <seealso cref="IWorkflowScript" />
        /// </summary>
        /// <param name="app"><see cref="Application"/></param>
        /// <param name="args"><see cref="WorkflowEventArgs"/></param>
        public void OnWorkflowScriptExecute(Hyland.Unity.Application app, Hyland.Unity.WorkflowEventArgs args)
        {
            app.Diagnostics.Level = DiagLevel;
            app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("Start Script: {0}", ScriptName));
            // We're setting ScriptResult to true
            // This becomes false, only if an Exception is caught
            args.ScriptResult = true;

            string partyID = string.Empty;

            try
            {
                // Get the current active document
                Document _currentDocument = args.Document;
                if (_currentDocument == null)
                    throw new ApplicationException("No active document was found!");

                string[] listOfPipes = System.IO.Directory.GetFiles(@"\\.\pipe\");

                foreach (var item in listOfPipes)
                {
                    app.Diagnostics.Write(string.Format("Pipe List Item: ", item));
                }

                KeywordType partyType = app.Core.KeywordTypes.Find("Party ID");
                if (partyType == null)
                    app.Diagnostics.Write("Could not find Party ID keyword type");

                KeywordRecord record = _currentDocument.KeywordRecords.Find(partyType);
                if (record == null)
                    app.Diagnostics.Write("Could not find keyword record containing the keyword type: Party ID");

                Keyword partyKey = record.Keywords.Find(partyType);
                if (partyType != null)
                {
                    partyID = partyKey.Value.ToString();
                }

                app.Diagnostics.Write("Party ID Value: " + partyID);

                if (Directory.Exists(pipePath))
                {
                    app.Diagnostics.Write(string.Format("Directory [{0}] exists.", pipePath));

                    SafeFileHandle driveHandle = CreateFile(pipePath, FileAccess.ReadWrite, FileShare.ReadWrite, IntPtr.Zero, FileMode.Create, FileAttributes.Normal, IntPtr.Zero);

                    if (driveHandle.IsInvalid)
                    {
                        throw new Win32Exception(Marshal.GetLastWin32Error());
                    }

                    // using will prevent the handle to be closed automatically
                    using (FileStream InStream = new FileStream(driveHandle, FileAccess.ReadWrite, 10000))
                    {
                        byte[] byteData = Encoding.ASCII.GetBytes("QuickSearchText: " + partyID);
                        app.Diagnostics.Write("Byte Data Length: " + byteData.Length);
                        //byte[] info = new UTF8Encoding(true).GetBytes("QuickSearchText: " + partyID);
                        if (InStream.CanWrite)
                        {
                            InStream.Write(byteData, 0, byteData.Length);
                        }
                    }
                }
                else
                {
                    app.Diagnostics.Write(string.Format("Directory [{0}] doesn't exist.", pipePath));
                }

                //System.IO.DirectoryInfo di = new System.IO.DirectoryInfo("\\\\.\\pipe");
                //FileInfo[] afi = di.GetFiles();

                //foreach (FileInfo fi in afi)
                //{
                //    app.Diagnostics.Write(string.Format("File Info: [{0}]", fi.ToString()));
                //}

                //string path = "\\\\.\\pipe\\" + PipeName;

                //if (File.Exists(path))
                //{
                //    app.Diagnostics.Write(string.Format("[{0}] File Exists: [{1}]", PipeName, File.Exists(path)));
                //    // Create a file to write to.
                //    using (System.IO.StreamWriter file = new System.IO.StreamWriter(path, true))
                //    {
                //        file.WriteLine("QuickSearchText: " + partyID);
                //    }
                //}
                //else
                //{
                //    app.Diagnostics.Write(string.Format("[{0}] File Exists: [{1}]", PipeName, File.Exists(path)));
                //    File.Create(path).Dispose();
                //    using (TextWriter tw = new StreamWriter(path, true))
                //    {
                //        tw.WriteLine("QuickSearchText: " + partyID);
                //    }
                //}

                if (partyID.Equals(string.Empty))
                {
                    args.ScriptResult = false;
                }
                else
                {
                    args.ScriptResult = true;
                }

            }
            catch (UnityAPIException ex)
            {
                HandleException("Unity API", ex, ref app, ref args);
            }
            catch (ApplicationException ex)
            {
                HandleException("Application", ex, ref app, ref args);
            }
            catch (Exception ex)
            {
                HandleException("General", ex, ref app, ref args);
            }
            finally
            {
                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", ScriptName));
                app.Disconnect();
            }
        }

        #endregion

        #region Helper Functions

        private void HandleException(string exceptionType, Exception ex, ref Application app, ref WorkflowEventArgs args)
        {
            args.ScriptResult = false;
            bool innerException = false;
            while (ex != null)
            {
                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Error,
                    string.Format("{0} Exception: {1}{2}{2}Stack Trace:{2}{3}", innerException ? "Inner" : exceptionType,
                        ex.Message, Environment.NewLine, ex.StackTrace));
                ex = ex.InnerException;
                innerException = true;
            }
        }

        #endregion
    }
}
