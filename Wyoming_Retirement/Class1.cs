﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wyoming_Retirement
{
    using System.Runtime.InteropServices;
    using Microsoft.Win32.SafeHandles;
    using System.IO;
    using System.ComponentModel;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    class Class1
    {
            // Import possibility to gather raw access to devices
    // with global \\.\ paths which is prohibited by normal
    // .NET <cref>FileStream</cref> class.
    [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    static extern SafeFileHandle CreateFile(
        string fileName,
        [MarshalAs(UnmanagedType.U4)] FileAccess fileAccess,
        [MarshalAs(UnmanagedType.U4)] FileShare fileShare,
        int securityAttributes,
        [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
        [MarshalAs(UnmanagedType.U4)] FileAttributes fileAttributes,
        IntPtr template);

    void MyUseFunction()
    {
        SafeFileHandle driveHandle = CreateFile(@"\\.\X:",
            FileAccess.Read,
            FileShare.ReadWrite, // drives must be opened with read and write share access
            0,
            FileMode.Open,
            FileAttributes.Normal,
            IntPtr.Zero);

        if (driveHandle.IsInvalid)
        {
            throw new Win32Exception(Marshal.GetLastWin32Error());
        }

        // using will prevent the handle to be closed automatically
        using (FileStream InStream = new FileStream(driveHandle, FileAccess.Read))
        {
            // do some stuff on the stream
        }
    }
    }
}
